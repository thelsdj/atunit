((last updated August 1st))

#Recruiting!

Hi!

So we are looking for anyone who would like to help build up a payment / funding site. We’re [project codename atunit](https://www.assembla.com/code/atunit/git/nodes/develop/README.md) and we’re creating a site to match [Paypal](http://paypal.com) / [Patreon](http://patreon.com). The difference is that our site will be run by queer people / people of color and focus on privacy and inclusion. Its intent is to be a community asset - as opposed to a capitalist enterprise - so we need people from all over different communities to help build it!

So what do we need exactly? Writers, website people, a few business people. But before moving on, it should be mentioned that this is a community based project and so we don’t have the capacity to pay people for things (**UNLESS YOU WANT TO JOIN AND PLAN A KICKSTARTER FOR US**).

---

People needed:

**Writers !**

There’s a lot of writing to be done for this project. In general all of the writing either falls under culture things (i.e. why does a funding site need to be specifically inclusive? what should our name be? how should we phrase [some sort of thing]) or project things (i.e. PR material and the FAQ).

**Website Developers / Designers !**

We are making a website so we need people who want to design and develop a website! Presently we need any sort of designers, and developers who are willing to help out with the early stage no-coding-being-written planning period.

**Business people !**

We are creating a formal / legal organization to support all of the money transfer things going on, and for the organization to run off of a cooperative governance model.

---

If you are interested in doing any of those things, or just want to ask questions, here ---> https://www.assembla.com/code/atunit/git/nodes/develop/README.md is where you should go for more information about that project!

Hopefully we’ll get a bunch of rad people on the project!

