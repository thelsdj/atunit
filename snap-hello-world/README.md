First Commit
============

Adding a Hello World example using SNAP.

To use this you need to "cabal install snap" to get the framework.

The initial contents of this was created with the command "snap init barebones".

To run it cd into the directory atunit/snap-hello-world and run "cabal build", this should create a binary ./dist/build/snap-hello-world/snap-hello-world - running it starts up a web server that just has a single Hello World page. You can also run it more easily using "cabal run".


Second Part
===========

Changed this to use a Snaplet App rather than SimpleHTTP server, this handles loading templates automatically.Added a test/example template snaplets/heist/templates/index.tpl - The app should display it when loaded.
