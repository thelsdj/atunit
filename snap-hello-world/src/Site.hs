{-# LANGUAGE FlexibleInstances
, OverloadedStrings
, TemplateHaskell #-}
module Site where

import Control.Applicative
import Database.PostgreSQL.Simple.FromRow
import qualified Data.Text as T
import Text.Digestive
import Text.Digestive.Heist
import Text.Digestive.Snap
import Snap.Core (redirect)
import Snap.Snaplet
import Snap.Snaplet.Heist
import Snap.Snaplet.PostgresqlSimple
import Snap.Snaplet.Auth
import Application


data Project = Project
               { title :: T.Text
               , description :: T.Text
               }

instance FromRow Project where
  fromRow = Project <$> field <*> field

instance Show Project where
  show (Project title description) =
    "Project { title: " ++ T.unpack title
    ++ ", description: " ++ T.unpack description
    ++ " }\n"

isNotEmpty :: T.Text -> Bool
isNotEmpty = not . T.null

titleErrMsg :: T.Text
titleErrMsg = "Title can not be empty"
descriptionErrMsg :: T.Text
descriptionErrMsg = "Description can not be empty"

projectForm :: (Monad m) => Form T.Text m Project
projectForm = Project
  <$> "title" .: check titleErrMsg isNotEmpty (text Nothing)
  <*> "description" .: check descriptionErrMsg isNotEmpty (text Nothing)

projectFormHandlerRestricted :: Handler App (AuthManager App) ()
projectFormHandlerRestricted = requireUser auth noUserHandler projectFormHandler

noUserHandler :: Handler App (AuthManager App) ()
noUserHandler = redirect "/login"

projectFormHandler :: Handler App (AuthManager App) ()
projectFormHandler = do
  (view, result) <- runForm "project" projectForm
  case result of
    Just x -> createNewProject x
    Nothing -> heistLocal (bindDigestiveSplices view) $ render "projectform"

createNewProject :: Project -> Handler App (AuthManager App) ()
createNewProject p = do
  _ <- execute "INSERT INTO projects VALUES (?, ?)" (title p, description p)
  render "index"


