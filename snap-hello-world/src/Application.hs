{-# LANGUAGE FlexibleInstances
, OverloadedStrings
, TemplateHaskell #-}

module Application where

import           Snap
import           Snap.Snaplet.Heist
import           Snap.Snaplet.Session
import           Snap.Snaplet.Auth
import           Control.Lens hiding (index)
import           Snap.Snaplet.PostgresqlSimple

-- Create the App object

data App = App
           { _heist :: Snaplet (Heist App)
           , _pg :: Snaplet Postgres
           , _sess :: Snaplet SessionManager
           , _auth :: Snaplet (AuthManager App)
           }

makeLenses ''App

instance HasHeist App where
  heistLens = subSnaplet heist

instance HasPostgres (Handler b App) where
  getPostgresState = with pg get

instance HasPostgres (Handler App (AuthManager App)) where
  getPostgresState = withTop pg get

