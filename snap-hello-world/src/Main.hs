{-# LANGUAGE FlexibleInstances
, OverloadedStrings
, TemplateHaskell #-}

module Main where

import           Snap
import           Snap.Snaplet.Heist
import           Snap.Snaplet.PostgresqlSimple
import qualified Data.ByteString.Char8 as B
import qualified Data.Text             as T
import qualified Heist.Interpreted     as I
import           Control.Lens hiding (index)
import           Snap.Snaplet.Auth.Backends.PostgresqlSimple
import           Snap.Snaplet.Session.Backends.CookieSession
import           Snap.Snaplet.Auth
import           Control.Monad.State.Class
import           Control.Applicative
import           Site
import           Application
import           Data.ByteString                             (ByteString)
import           Heist
import           Snap.Core
import           Snap.Snaplet
import           Snap.Snaplet.Auth
import           Snap.Util.FileServe


-- Build a routing table that describes how URLs are handled

routes :: [(B.ByteString, Handler App App ())]
routes = [ ("/", index)
         , ("/login",     with auth handleLoginSubmit)
         , ("/logout",    with auth handleLogout)
         , ("/new_user",  with auth handleNewUser)
         , ("/project",   with auth projectFormHandlerRestricted)
         ]

app :: SnapletInit App App
app = makeSnaplet "app" "My stunningly advanced Snap application." Nothing $ do
  h <- nestSnaplet "heist" heist $ heistInit "templates"
  p <- nestSnaplet "pg" pg pgsInit
  s <- nestSnaplet "sess" sess $
    initCookieSessionManager "site_key.txt" "sess" (Just 3600)
  a <- nestSnaplet "auth" auth $
    initPostgresAuth sess p
  addRoutes routes
  addAuthSplices h auth
  return $ App h p s a

index :: Handler App App ()
index = render "index"

handleLogin :: Maybe T.Text -> Handler App (AuthManager App) ()
handleLogin authError = heistLocal (I.bindSplices errs) $ render "login"
  where
    errs = maybe noSplices splice authError
    splice err = "loginError" ## I.textSplice err

handleLoginSubmit :: Handler App (AuthManager App) ()
handleLoginSubmit =
    loginUser "login" "password" Nothing
              (\_ -> handleLogin err) (redirect "/")
  where
    err = Just "Unknown user or password"

handleNewUser :: Handler App (AuthManager App) ()
handleNewUser = method GET handleForm <|> method POST handleFormSubmit
  where
    handleForm = render "new_user"
    handleFormSubmit = registerUser "login" "password" >> redirect "/"

handleLogout :: Handler App (AuthManager App) ()
handleLogout = logout >> redirect "/"

getAllProjects :: Handler App App ()
getAllProjects = do
  allProjects <- query_ "SELECT * FROM projects"
  liftIO $ print (allProjects :: [Project])

deleteProjectByTitle :: Handler App App ()
deleteProjectByTitle = do
  ti <- getPostParam "title"
  _ <- execute "DELETE FROM projects WHERE title = ?" (Only ti)
  redirect "/"

-- Startup the server with this configuration

main :: IO ()
main = do serveSnaplet defaultConfig app
